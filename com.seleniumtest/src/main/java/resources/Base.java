package resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class Base {
	
	public WebDriver driver;


	public WebDriver initializeDriver() throws IOException
	{
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\dtpr196\\eclipse-workspace\\com.seleniumtest\\src\\main\\java\\resources\\data.properties");
		
		prop.load(fis);
		String browserName = prop.getProperty("browsertype");
		System.out.println(browserName);
		if(browserName.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\dtpr196\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
			 driver = new ChromeDriver();
			
		}
		else if(browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\dtpr196\\Downloads\\geckodriver-v0.31.0-win64\\geckodriver.exe");
			 driver = new ChromeDriver();
			
		}
		else if(browserName.equalsIgnoreCase("msedge")) {
			
			System.setProperty("webdriver.edge.driver", "C:\\Users\\dtpr196\\Downloads\\edgedriver_win64\\msedgedriver.exe");	
			 driver = new EdgeDriver();
		}
		else if(browserName=="IE") {
			
		}
		
		
		return driver;
	}
}
