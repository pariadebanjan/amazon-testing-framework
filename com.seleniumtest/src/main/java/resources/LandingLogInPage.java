package resources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingLogInPage {
	
	public WebDriver driver;
	
	By signIn = By.xpath("//a[@id='nav-link-accountList']");
	
	public LandingLogInPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		
	}

	public void getLogIn() {
		driver.get("https://www.amazon.in/");
		
		 driver.findElement(signIn).click();
	}

}
