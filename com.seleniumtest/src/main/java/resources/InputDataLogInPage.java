package resources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InputDataLogInPage {
	
	WebDriver driver;
	
	By inputEmail = By.xpath("//input[@id='ap_email']");
	By clickContinuebtn = By.xpath("//span[@id='continue']");

	public InputDataLogInPage(WebDriver driver) {
		this.driver=driver;
	}

	public void inputEmail() {
		
		driver.findElement(inputEmail).sendKeys("6294214258");
		driver.findElement(clickContinuebtn).click();
	}
	
}
