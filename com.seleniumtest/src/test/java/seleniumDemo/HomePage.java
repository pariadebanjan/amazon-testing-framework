package seleniumDemo;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import resources.Base;
import resources.InputDataLogInPage;
import resources.LandingLogInPage;
import resources.PasswordLogInPage;

public class HomePage extends Base{

	@Test
	
	public void goToHomePage() throws IOException{
		
		driver = initializeDriver();
		
		
		//Thread.sleep(2000);
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// one is inheritance;
		// creating object to that class and invoke the methods of it;
		
		LandingLogInPage lip = new LandingLogInPage(driver);
		
		lip.getLogIn();
		
		InputDataLogInPage idlp = new InputDataLogInPage(driver);
		
		idlp.inputEmail();
		
		
		PasswordLogInPage plip = new PasswordLogInPage(driver);
		
		plip.passwordClick();
		
		
	}

	
}
